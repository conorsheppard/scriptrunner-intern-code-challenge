package com.adaptavist

import java.util.regex.Pattern

Pattern domainRegex
final String DOMAIN_NAME_PATTERN = '^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$'

String urlLink = "http://localhost:8080/jira/rest/scriptrunner/latest/custom/getEmailDomains"
ArrayList<String> domainList = EndpointTest.testEmailEndpoint(urlLink)

domainRegex = Pattern.compile(DOMAIN_NAME_PATTERN)

// Iterates over email domains to verify their validity
domainList.each {
    domainName -> println "$domainName is a valid domain name: ${(domainRegex.matcher(domainName).find())}"
}

