package com.adaptavist

import groovy.json.JsonSlurper


class EndpointTest {

    static void main(String[] args) {
        String urlLink = "http://localhost:8080/jira/rest/scriptrunner/latest/custom/getEmailDomains"
        ArrayList<String> domains = testEmailEndpoint(urlLink)
        domains.each {
            domain -> println(domain)
        }
    }

    static ArrayList<String> testEmailEndpoint(String urlLink) {
        InputStream inputStream = null
        URL url = new URL(urlLink)
        HttpURLConnection con
        con = (HttpURLConnection) url.openConnection()
        con.setRequestMethod("GET")
        inputStream = con.getInputStream()
        ArrayList<String> domains = readResponse(inputStream)
        return domains
    }

    // Reads response from the server and returns the email domains as a list
    static ArrayList<String> readResponse(InputStream inputStream) throws IOException {
        JsonSlurper slurper = new JsonSlurper()
        def response = slurper.parse(inputStream, "UTF-8")
        ArrayList<String> domainList = new ArrayList<>()
        response.each {
            result ->
                domainList.add(result.toString())
        }

        return domainList
    }
}
