package com.adaptavist

import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import com.atlassian.jira.bc.user.search.UserSearchService
import com.atlassian.jira.bc.user.search.UserSearchParams
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser
import groovy.transform.BaseScript
import javax.ws.rs.core.Response
import groovy.json.JsonBuilder

@BaseScript CustomEndpointDelegate delegate

// For the sake of simplifying the tests, I have omitted authentication on this endpoint
getEmailDomains(httpMethod: "GET") {

    UserSearchService userSearchService = ComponentAccessor.getComponent(UserSearchService)
    UserSearchParams userSearchParams = new UserSearchParams(true, true, true)
    // Get users as list
    List<ApplicationUser> userList = userSearchService.findUsers("", userSearchParams)

    // Store email domains in a set to avoid duplicates
    Set<String> emailDomainSet = new HashSet<>()
    String emailDomain = ""
    // Iterate over list of user email addresses and truncate them to just the domain name
    userList.each { user ->
        String emailAddress = user.getEmailAddress()
        emailDomain = emailAddress.substring(emailAddress.indexOf('@')+1, emailAddress.length())
        emailDomainSet.add(emailDomain)
    }

    return Response.ok(new JsonBuilder(emailDomainSet).toPrettyString()).build()
}
