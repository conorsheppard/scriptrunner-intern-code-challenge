# ScriptRunner Intern Coding Challenge #

## Notes on submission: Conor Sheppard - 19/03/2017 ##


**EmailDomainsEndpoint**

To get all of the users in the JIRA instance I used *UserSearchService* and stored the result as a list. I then iterated over the list and used a Set to store the email domains and guarantee uniqueness. I then return this set as a JSON object.

I was unable to run the Spock test file in Intellij and therefore wrote the following endpoint test and test script in the test folder.

**EndpointTest Class**

If you have four users in the system, the admin and say three other users with emails john@atlassian.com, conor@google.com and gerry@adaptavist.com, then the output of the EndpointTest class’s main method when invoked would be:

>
atlassian.com

>
google.com

>
adaptavist.com

>
admin.com

**TestDomainFormat Script**

This script is designed to run the static testEmailEndpoint method present in EndpointTest and verify that all of the domain names in the resulting list are valid. If again run for the above users the output would be:

![Screen Shot 2017-03-19 at 18.16.52.png](https://bitbucket.org/repo/jgEKpk7/images/1308457314-Screen%20Shot%202017-03-19%20at%2018.16.52.png)

**Caveats**

While possibly an oversight on my part, I had to use the _Script Console_ in my JIRA instance to add the script via its relative path as it was not automatically installed as outlined below in the "Using the Atlassian Plugin SDK Command Line" section, even after running atlas-package while atlas-debug was running. 

**Issues**

All tests and additional classes do run as expected/described although I was witnessing some very odd behaviour in Intellij (getting errors such as java.lang.ClassNotFoundException and errors from code that had since been removed). I invalidated the cache and did a restart but this had no effect. I then tested the files in a separate project and they ran fine - this could simply be a problem with my workstation however.

## Purpose ##

The purpose of this repository is to serve as a test for those who want to join the Adaptavist ScriptRunner team's
internship program.

## Prerequisites ##

You'll need: 

* the programming knowledge of roughly a Junior-level Computer Science student 
* a willingness to hack through new frameworks (unless you are already a ScriptRunner guru, in which case, awesome!)
* to run some things from the command line / terminal of your choice

Additionally, exposure to Groovy, the Java programming language, Atlassian JIRA, and git would all be helpful, but are 
by no means prerequisites; it's fine, and even expected that you'll learn some of these things as you work on 
the challenge.

###Software to Install###

Java JDK 8 (tested with Oracle 1.8_102)

Atlassian Plugin SDK. The challenge has been tested with version 6.2.9, amps version 6.2.6.

* [Windows Version](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project/install-the-atlassian-sdk-on-a-windows-system)
* [Linux / Mac OSX](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project/install-the-atlassian-sdk-on-a-linux-or-mac-system)

We'd also strongly encourage you to setup [a ScriptRunner development environment](https://scriptrunner.adaptavist.com/latest/jira/DevEnvironment.html),
which will include IntelliJ IDEA Community Edition.
It takes a bit more time, and isn't strictly required to complete the challenge, but it is likely to prove extremely
helpful in completing the challenge.

If you are uncomfortable with command-line git, a GUI git client like [SourceTree](https://www.sourcetreeapp.com/) may
be helpful for committing and pushing your changes.

## Challenge Requirements ##

### Core Requirement ###

The core requirement (and the only one necessary to complete for this challenge) is to write a 
[Scripted REST Endpoint](https://scriptrunner.adaptavist.com/latest/jira/rest-endpoints.html) that returns a list of
 all the distinct email domains for the set of all users in a JIRA instance. For example, if there are four users in
 JIRA with the following email addresses:
 
 * guillaume@gmail.com
 * khadija@gmail.com
 * sue@adaptavist.com
 * admin@example.com
 
The REST Endpoint should return the the strings gmail.com, adaptavist.com, and example.com.

The code should go in the src/main/resources/com/adaptavist/EmailDomainsEndpoint.groovy file. The endpoint should
automatically be available when running the plugin code, so that someone could clone your source code, run
`atlas-clean; atlas-debug`
and then visit http://localhost:8080/jira/rest/scriptrunner/latest/custom/<your name for the REST endpoint> and get a response.
If they add users with other email addresses and reload the URL, those domains should display on the next refresh. See
the guide on [Creating a script plugin](https://scriptrunner.adaptavist.com/latest/jira/creating-a-script-plugin.html).

## Getting Started ##

To give a rough, step-by-step overview of the development process:

* Fork the repository
* Clone the code from your fork
* In a terminal, `cd` into the local source directory and run the `atlas-debug` command to start a local instance of JIRA
* Access the locally running JIRA; The default username and password are both admin.
* Configure a Scripted REST Endpoint that points to the file in your project's code
* Write the code so that your REST Endpoint returns the expected results; run atlas-package in another terminal in 
    the project directory after you save changes. This will automatically repackage your code so that your endpoint 
    returns updated results
* Configure your plugin so that the REST Endpoint automatically gets added to JIRA's configuration on a clean startup 
    (that is, after stopping JIRA, running atlas-clean to wipe out the local JIRA, then running atlas-debug to start a
     fresh instance)
* Once all that is done, push your changes to your fork. Grant Adaptavist access to the repository.
Also, add the person you've been in touch with about the internship. If you can't find that person, add 
Jonny Carter (`jonnycarteradaptavist`) and Jamie Echlin (`jechlin`).

#### Tips ####

* If you get stuck staring at a blank editor, try taking an example REST Endpoint from [the ScriptRunner 
documentation](https://scriptrunner.adaptavist.com/latest/jira/rest-endpoints.html). Get the basic example running,
then tweak it until you get the results you want. 
* [IntelliJ IDEA's autocomplete](https://www.jetbrains.com/help/idea/2016.3/auto-completing-code.html) is of great 
help. Use it!
* You'll probably want to configure the REST Endpoint through the GUI, then export the needed YAML for the 
src/main/resources/scriptrunner.yaml file using the Configuration Exporter Built-in Script.
* Make sure to save your code in the project files so it doesn't get wiped out if you run `atlas-clean`.
* Make git commits as you make progress, even if the code isn't 100% complete. The ~~paranoid~~ wise will
push their changes to their forked repository in case something happens to their local copy.

### Extensions for over-achievers ###

It is not necessary to attempt any of these extension requirements. If you do attempt them, they are not in order and 
you can pick and choose which ones you complete

1. Write an integration test to test the REST Endpoint.
The code in this repository contains a basic test for the endpoint in src/test/groovy/com/adaptavist/EmailDomainsEndpointSpec.groovy.
See the [ScriptRunner testing guide](https://scriptrunner.adaptavist.com/latest/jira/testing.html) for some guidance on
crafting and running tests. Also check out the [docs for the Spock framwork](http://spockframework.org/).
1. Add some other ScriptRunner functionality that consumes the REST Endpoint, or shares some code with it.
For example, you might add a [Behaviour](https://scriptrunner.adaptavist.com/latest/jira/behaviours-overview.html) 
that uses the endpoint to populate a drop-down menu with a list of email domains.
1. Write a functional tests using Geb. This is similar to writing an integration test, only the tests will
actually fire up a web browser and visit URLs. You'll need to install and start chromedriver manually before running
the test. Your test should extend the geb.spock.GebReportingSpec. The provided GebConfig.groovy file in 
src/test/groovy/com/adaptavist/GebConfig.groovy should be enough for the configuration, but you'll need to create
the test class file.
1. Anything else you can think of. Be creative!

## How to Submit ##

1. Push your changes to your fork. Grant Adaptavist access to the repository. Also, add the person you've been in touch 
with about the internship. If you can't find that person, add Jonny Carter (`jonnycarteradaptavist`) and Jamie Echlin 
(`jechlin`).
1. Email the person who gave you this challenge or `recruitment` at `adaptavist.com` with a link to your fork.

## Using the Atlassian Plugin SDK Command Line ##

Here are the SDK commands you'll use during development:

* atlas-debug -- this will build the add-on that has your code in it, startup JIRA, and install your add-on
  * The first run will probably take a while, as it has to download a whole bunch of dependencies.
    Fear not! Eventually you'll see something like 
    ```
    [INFO] jira started successfully in 89s at http://compyx86:8080/jira
    [INFO] Type Ctrl-D to shutdown gracefully
    [INFO] Type Ctrl-C to exit
    ```
    cross your screen. Once it does, you can visit http://localhost:8080/jira in your web browser and start poking
    at JIRA. You can open a new terminal to run other atlas commands or git commands while atlas-debug continues to run.
* atlas-package -- builds the plugin; if you run this in a new terminal while atlas-debug is running in another, 
                   the newly packaged plugin should automatically get installed in your local JIRA instance.
                   You'll need to do this as you make changes to your REST Endpoint's code.
* atlas-clean -- This will wipe out your local JIRA instance entirely, including
                 any configuration you made via the GUI; make sure you've saved any inline scripts to a file, and 
                 that you've backed up anything you care about. Don't run it when atlas-debug is running.

The `atlas-help` will print a description for all commands in the SDK if you feel like mucking around a bit.

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK

## Getting help ##
You will probably have questions. That's normal! Some good resources include:

* The [ScriptRunner documentation](https://scriptrunner.adaptavist.com/latest/jira/)
* The [JIRA JavaDoc](https://docs.atlassian.com/jira/server/)
* [Atlassian Answers](https://answers.atlassian.com/questions/topics)
* [Groovy documentation](http://www.groovy-lang.org/documentation.html) and [StackOverflow](http://stackoverflow.com/) 
  for guidance on how to do something in Groovy
* The web search provider of your choice. :)

Also, feel free to just ask the person who contacted you (probably Jonny).